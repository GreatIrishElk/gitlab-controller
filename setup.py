from setuptools import setup, find_packages

setup(
        name='gitlab-controller',
        version='0.0.1',
        author='Simon Murphy',
        author_email='murphysimon@outlook.com',
        classifers=[
            "Programming Language :: Python :: 3",
        ],
        packages=find_packages(),
        python_requires='>=3.6',
        install_requires=["python-gitlab"]
)
