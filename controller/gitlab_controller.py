import gitlab


class GitlabController:

    def __init__(self, private_token, url='https://gitlab.com/'):
        self._gitlab = gitlab.Gitlab(url, private_token=private_token)
        self._gitlab.auth()

    @property
    def gitlab(self):
        return self._gitlab

    def list_repositories(self):
        for repository in self.get_repositories():
            print(
                f"Name: {repository.name}"
                f"Owner: {repository.owner['username']}"
                f"SSH URL: {repository.ssh_url_to_repo}"
                f"HTTP URL: {repository.http_url_to_repo}"
            )

    def get_repositories(self, visibility='public'):
        return self.gitlab.projects.list(visibility=visibility, owned=True)

    def get_repository(self, repository):
        for repo in self.get_repositories():
            if repo.name == repository:
                return repo

    def get_project(self, project_name, visibility='private'):
        projects = self.gitlab.projects.list(visibility=visibility, all=True)
        print("Returned projects {}".format([x.name for x in projects]))
        project = None
        for project in projects:
            if project.name == project_name:
                break
            else:
                project = None
        return project

    @staticmethod
    def get_pipelines(project, branch, status='success', items_per_page=100):
        pipelines = []
        for pipeline in project.pipelines.list(per_page=items_per_page):
            if pipeline.attributes['ref'] in branch and pipeline.attributes['status'] in status:
                pipelines.append(pipeline)
        return pipelines

    def get_pipeline(self, project, branch, status='success', items_per_page=100):
        pipelines = self.get_pipelines(project, branch, status, items_per_page)
        pipeline = None
        for pipeline in pipelines:
            if pipeline.attributes['ref'] == branch in pipeline.attributes['status'] in status:
                break
            else:
                pipeline = None
        return pipeline

    @staticmethod
    def get_job(pipeline, stage, status='success'):
        jobs = pipeline.jobs.list()
        job = None
        for job in jobs:
            if job.attributes['name'] == stage and job.attributes['status'] in status:
                break
            else:
                job = None
        return job

    @staticmethod
    def download_job_artifacts(job, project, filename="artifacts.zip"):
        """

        :param job: Gitlab job to download artifact from.
        :param project: Project to download artifact from.
        :param filename: name to save downloaded file as.
        """
        artifacts_job = project.jobs.get(job.id, lazy=True)
        with open(filename, "wb") as f:
            artifacts_job.artifacts(streamed=True, action=f.write)

    @staticmethod
    def download_job_artifact(job, project, path_to_artifact, filename):
        """

        :param job: Gitlab job to download artifact from.
        :param project: Project to download artifact from.
        :param path_to_artifact: path to artifact location within artifacts in gitlab job.
        :param filename: name to save downloaded file as.
        """
        artifacts_job = project.jobs.get(job.id, lazy=True)
        with open(filename, "wb") as f:
            artifacts_job.artifact(path_to_artifact, streamed=True, action=f.write)
