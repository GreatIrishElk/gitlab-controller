from scripts.gitlab_controller import GitlabController
import sys

private_token = sys.argv[0]


def main():
    gl = GitlabController(private_token)
    gl.list_repositories()


if __name__ == '__main__':
    main()
